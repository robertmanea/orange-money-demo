//
//  CurrencyTests.swift
//  Orange Money DemoTests
//
//  Created by Robert Manea on 07/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import XCTest
@testable import Orange_Money_Demo

class CurrencyTests: XCTestCase {

    func testFormattingRate() {
        let eur = Currency(name: "EUR", value: 4.098)
        let hpvm = HomePageViewModel(homePageViewController: HomePageViewController())
        hpvm.currencies.append(eur)
        let rateStr = hpvm.getRateString(forCurrencyAt: 0)
        
        XCTAssertEqual("EUR - 4.098", rateStr)
    }
}
