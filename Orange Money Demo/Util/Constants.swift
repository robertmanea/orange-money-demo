//
//  Constants.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 08/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

struct Constants {
    struct Errors {
        static let noInternetError = "Could not communicate with the server. Please make sure you have a stable internet connection."
        static let serverError = "Could not get a valid response from the server. Please try again later."
        static let selectHistoryCurrenciesError = "You can't select more than 3 currencies to be displayed in the history chart!"
    }
    
    struct ViewControllers {
        static let home = "HomePageViewController"
        static let history = "HistoryViewController"
        static let settings = "SettingsViewController"
        static let menu = "MenuViewController"
        static let multiselect = "MultiSelectionTableViewController"
    }
    
    struct UserDefaults {
        static let baseCurrency = "baseCurrency"
        static let historyDays = "historyDays"
        static let refreshRate = "homePageRefreshRate"
        static let historyCurrencies = "historyCurrencies"
    }
    
    struct PickerModes {
        static let currency = "currency"
        static let refreshRate = "refreshRate"
        static let historyDays = "historyDays"
    }
}
