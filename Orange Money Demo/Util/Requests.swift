//
//  Requests.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 26/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit
import Alamofire
import Reachability

class Requests: NSObject {
    static let HOST = "https://api.exchangeratesapi.io/"
    
    static func hasInternetConnection() -> Bool {
        return Reachability.forInternetConnection()?.currentReachabilityStatus().rawValue != 0
    }
    
    static func getLatestExchangeRates(completion: @escaping (_ currencies: [Currency]?, _ message: String?) -> Void) {
        if hasInternetConnection() {
            let base = AppSession.sharedSession.baseCurrency
            let url = URL(string: "\(HOST)latest?base=\(base)")
            
            AF.request(url!).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    do {
                        let currenciesResponse = try JSONDecoder().decode(RatesResponse.self, from: response.data!)
                    
                        //need to populate the known currencies, but the API doesn't provide such a request
                        //resorted to getting them when first executing the "latest rates" request
                        if AppSession.sharedSession.currencies.isEmpty {
                            AppSession.sharedSession.currencies.append(contentsOf: currenciesResponse.currencies.map({ (elem) -> String in
                                return elem.name
                            }))

                            if !AppSession.sharedSession.currencies.contains(base) {
                                AppSession.sharedSession.currencies.append(base)
                            }

                            AppSession.sharedSession.currencies.sort(by: { (s1, s2) -> Bool in
                                return s1.compare(s2) == .orderedAscending
                            })
                        }
                        
                        completion(currenciesResponse.currencies, nil)
                    } catch {
                        completion(nil, Constants.Errors.serverError)
                    }
                    
                    break
                case .failure(let err):
                    completion(nil, err.localizedDescription)
                    break
                }
            })
        }
        else {
            completion(nil, Constants.Errors.noInternetError)
        }
    }
    
    static func getHistory(completion: @escaping (_ currencies: [String:[Currency]]?, _ message: String?) -> Void) {
        if hasInternetConnection() {
            let base = AppSession.sharedSession.baseCurrency
            let historyCurrencies = AppSession.sharedSession.historyCurrencies.reduce("") { (res, str) -> String in
                return res == "" ? str : res + "," + str
            }
            let days = AppSession.sharedSession.historyDays
            
            //API only returns rates for business days (Mon - Fri)
            //the chart will display values for the previous 10 days, including weekends
            //for weekends, we should show the rate on the business day prior to that weekend, so we'll manipulate the start_at date to be even earlier than 10 days by only counting business days
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd"
            let calendar = Calendar.current
            let endDateStr = dF.string(from: Date())
            var startDate = calendar.date(byAdding: .day, value: -1, to: Date())
            
            for _ in 0..<days {
                while (calendar.isDateInWeekend(startDate!)) {
                    startDate = calendar.date(byAdding: .day, value: -1, to: startDate!)
                }

                startDate = calendar.date(byAdding: .day, value: -1, to: startDate!)
            }

            while (calendar.isDateInWeekend(startDate!)) {
                startDate = calendar.date(byAdding: .day, value: -1, to: startDate!)
            }
            
            let startDateStr = dF.string(from: startDate!)
            
            let url = URL(string: "\(HOST)history?base=\(base)&symbols=\(historyCurrencies)&start_at=\(startDateStr)&end_at=\(endDateStr)")
            
            AF.request(url!).responseJSON { (response) in
                switch response.result {
                case .success:
                    do {
                        let historyResponse = try JSONDecoder().decode(HistoryResponse.self, from: response.data!)

                        completion(historyResponse.historyDict, nil)
                    }
                    catch {
                        completion(nil, Constants.Errors.serverError)
                    }
                    
                    break
                case .failure(let err):
                    completion(nil, err.localizedDescription)
                    break
                }
            }
        }
        else {
            completion(nil, Constants.Errors.noInternetError)
        }
    }
}
