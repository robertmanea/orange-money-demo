//
//  ExchangeRateValueFormatter.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 01/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit
import Charts

class ExchangeRateValueFormatter: NSObject, IValueFormatter {

    //needed a custom formatter, as the original one only displayed one decimal
    open func stringForValue(
        _ value: Double,
        entry: ChartDataEntry,
        dataSetIndex: Int,
        viewPortHandler: ViewPortHandler?) -> String
    {
        return String(format: "%.4f", value)
    }
}
