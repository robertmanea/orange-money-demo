//
//  AppSession.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 26/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class AppSession: NSObject {
    var currencies = [String]()
    var baseCurrency = "EUR"
    var homePageRefreshRate = 3
    var historyDays = 10
    var historyCurrencies = ["RON","USD","BGN"]
    
    static let sharedSession: AppSession = {
        var session = AppSession()
        session.baseCurrency = UserDefaults.standard.object(forKey: Constants.UserDefaults.baseCurrency) as? String ?? "EUR"
        session.homePageRefreshRate = UserDefaults.standard.value(forKey: Constants.UserDefaults.refreshRate) as? Int ?? 3
        session.historyDays = UserDefaults.standard.value(forKey: Constants.UserDefaults.historyDays) as? Int ?? 10
        session.historyCurrencies = UserDefaults.standard.object(forKey: Constants.UserDefaults.historyCurrencies) as? [String] ?? ["RON","USD","BGN"]
        return session
    }()
}
