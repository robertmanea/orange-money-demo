//
//  BaseViewModel.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 08/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject {
    var baseViewController: BaseViewController
    let appDel = UIApplication.shared.delegate as! AppDelegate

    init(baseViewController: BaseViewController) {
        self.baseViewController = baseViewController
    }

    func getBaseCurrency() -> String {
        return "Base Currency: \(AppSession.sharedSession.baseCurrency)"
    }
    
    func getBaseCurrencyLabelBottomY() -> CGFloat {
        return baseViewController.baseCurrencyLabel.frame.origin.y + baseViewController.baseCurrencyLabel.frame.size.height
    }
    
    func getErrorLabelBottomY() -> CGFloat {
        return baseViewController.errorLabel.frame.origin.y + baseViewController.errorLabel.frame.size.height
    }
}
