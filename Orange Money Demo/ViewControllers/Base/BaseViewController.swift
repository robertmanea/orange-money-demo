//
//  BaseViewController.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 30/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    @IBOutlet weak var baseCurrencyLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    var baseViewModel: BaseViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        baseViewModel = BaseViewModel(baseViewController: self)
        navigationController?.navigationBar.tintColor = .black
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(openMenu))
        navigationItem.title = "Orange Money Demo"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if baseCurrencyLabel != nil {
            baseCurrencyLabel.text = baseViewModel!.getBaseCurrency()
        }
    }
    
    func showErrorLabel() {
        UIView.animate(withDuration: 0.2) {
            self.errorLabel.frame.origin.y = self.view.safeAreaInsets.top
            self.arrangeViews()
        }
    }
    
    func hideErrorLabel() {
        UIView.animate(withDuration: 0.2) {
            self.errorLabel.frame.origin.y = self.view.safeAreaInsets.top - self.errorLabel.frame.size.height
            self.arrangeViews()
        }
    }
    
    func arrangeViews() {
        if baseCurrencyLabel != nil && errorLabel != nil {
            baseCurrencyLabel.frame.origin.y = baseViewModel!.getErrorLabelBottomY()
        }
    }
    
    @objc func openMenu() {
        appDel.sideMenuController?.showLeftViewAnimated()
    }
    
    func pushViewController(viewController: UIViewController, animated: Bool = true) {
        DispatchQueue.main.async {
            let nav = self.appDel.sideMenuController?.rootViewController as! UINavigationController
            
            if nav.viewControllers.contains(viewController) {
                if nav.topViewController != viewController {
                    nav.popToViewController(viewController, animated: animated)
                }
            }
            else {
                nav.pushViewController(viewController, animated: animated)
            }
            
            self.appDel.sideMenuController?.hideLeftViewAnimated()
        }
    }
}
