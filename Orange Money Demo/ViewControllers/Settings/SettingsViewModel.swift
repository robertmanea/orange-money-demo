//
//  SettingsViewModel.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 08/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class SettingsViewModel: BaseViewModel {
    var settingsViewController: SettingsViewController
    
    init(settingsViewController: SettingsViewController) {
        self.settingsViewController = settingsViewController
        super.init(baseViewController: settingsViewController)
    }
    
    func getRefreshRateString() -> String {
        return "\(AppSession.sharedSession.homePageRefreshRate)s"
    }
    
    func getHistoryDaysString() -> String {
        return "\(AppSession.sharedSession.historyDays)"
    }
    
    func getHistoryCurrenciesString() -> String {
        return AppSession.sharedSession.historyCurrencies.reduce("", { (res, str) -> String in
            return res == "" ? str : res + ", " + str
        })
    }
    
    func initializeMultiSelectionTableViewController() -> MultiSelectionTableViewController {
        let table = MultiSelectionTableViewController.init(nibName: Constants.ViewControllers.multiselect, bundle: nil)
        table.options = AppSession.sharedSession.currencies
        if let idx = table.options.firstIndex(of: AppSession.sharedSession.baseCurrency) {
            table.options.remove(at: idx)
        }
        
        table.selectedOptions = AppSession.sharedSession.historyCurrencies
        
        return table
    }
    
    func setNewBaseCurrency(currencyName: String) {
        let oldBaseCurrency = AppSession.sharedSession.baseCurrency
        if let titleIndex = AppSession.sharedSession.historyCurrencies.firstIndex(of: currencyName) {
            AppSession.sharedSession.historyCurrencies.remove(at: titleIndex)
            AppSession.sharedSession.historyCurrencies.append(oldBaseCurrency)
            UserDefaults.standard.set(AppSession.sharedSession.historyCurrencies, forKey: Constants.UserDefaults.historyCurrencies)
        }
        
        AppSession.sharedSession.baseCurrency = currencyName
        UserDefaults.standard.set(AppSession.sharedSession.baseCurrency, forKey: Constants.UserDefaults.baseCurrency)
        UserDefaults.standard.synchronize()
    }
    
    func setNewRefreshRate(refreshRate: String) {
        AppSession.sharedSession.homePageRefreshRate = Int(refreshRate.components(separatedBy: " ").first!)!

        UserDefaults.standard.set(AppSession.sharedSession.homePageRefreshRate, forKey: Constants.UserDefaults.refreshRate)
        UserDefaults.standard.synchronize()
    }
    
    func setNewHistoryDays(historyDays: String) {
        AppSession.sharedSession.historyDays = Int(historyDays)!
        UserDefaults.standard.set(AppSession.sharedSession.historyDays, forKey: Constants.UserDefaults.historyDays)

        UserDefaults.standard.synchronize()
    }
}
