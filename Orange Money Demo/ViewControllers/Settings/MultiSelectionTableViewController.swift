//
//  MultiSelectionTableViewController.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 07/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class MultiSelectionTableViewController: UITableViewController {
    
    var options = [String]()
    var selectedOptions = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = true
        self.tableView.allowsMultipleSelection = true

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(tapDone))
    }

    @objc func tapDone() {
        AppSession.sharedSession.historyCurrencies = selectedOptions
        
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MultiSelectIdentifier"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        }
        
        let currency = options[indexPath.row]
        cell?.textLabel?.text = currency
        cell?.selectionStyle = .gray
        
        if selectedOptions.contains(currency) {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }

        return cell!
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedOptions.count == 3 {
            tableView.deselectRow(at: indexPath, animated: true)
            
            let alert = UIAlertController(title: "Error", message: Constants.Errors.selectHistoryCurrenciesError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        let currency = options[indexPath.row]
        if !selectedOptions.contains(currency) {
            selectedOptions.append(currency)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let currency = options[indexPath.row]
        
        if let index = selectedOptions.firstIndex(of: currency) {
            selectedOptions.remove(at: index)
        }
    }
}
