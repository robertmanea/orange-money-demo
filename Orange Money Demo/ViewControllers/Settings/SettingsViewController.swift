//
//  SettingsViewController.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 02/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var baseCurrencyContainerView: UIView!
    @IBOutlet weak var baseCurrencySelectionLabel: UILabel!
    @IBOutlet weak var refreshRateContainerView: UIView!
    @IBOutlet weak var refreshRateSelectionLabel: UILabel!
    @IBOutlet weak var historyDaysContainerView: UIView!
    @IBOutlet weak var historyDaysSelectionLabel: UILabel!
    @IBOutlet weak var historyCurrenciesContainerView: UIView!
    @IBOutlet weak var historyCurrenciesSelectionLabel: UILabel!
    @IBOutlet weak var pickerContainerView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var pickerMode = Constants.PickerModes.currency
    var refreshRates = ["3 seconds", "5 seconds", "15 seconds"]
    var historyDays = ["3", "5", "10"]
    var settingsViewModel: SettingsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsViewModel = SettingsViewModel(settingsViewController: self)
        pickerView.delegate = self
        pickerView.dataSource = self
        
        let tapCurrency = UITapGestureRecognizer(target: self, action: #selector(selectCurrency))
        baseCurrencyContainerView.isUserInteractionEnabled = true
        baseCurrencyContainerView.addGestureRecognizer(tapCurrency)
        
        let tapRefreshRate = UITapGestureRecognizer(target: self, action: #selector(selectRefreshRate))
        refreshRateContainerView.isUserInteractionEnabled = true
        refreshRateContainerView.addGestureRecognizer(tapRefreshRate)
        
        let tapHistoryDays = UITapGestureRecognizer(target: self, action: #selector(selectHistoryDays))
        historyDaysContainerView.isUserInteractionEnabled = true
        historyDaysContainerView.addGestureRecognizer(tapHistoryDays)
        
        let tapHistoryCurrencies = UITapGestureRecognizer(target: self, action: #selector(selectHistoryCurrencies))
        historyCurrenciesContainerView.isUserInteractionEnabled = true
        historyCurrenciesContainerView.addGestureRecognizer(tapHistoryCurrencies)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        baseCurrencySelectionLabel.text = AppSession.sharedSession.baseCurrency
        refreshRateSelectionLabel.text = settingsViewModel!.getRefreshRateString()
        historyDaysSelectionLabel.text = settingsViewModel!.getHistoryDaysString()
        historyCurrenciesSelectionLabel.text = settingsViewModel!.getHistoryCurrenciesString()
        
        baseCurrencySelectionLabel.sizeToFit()
        refreshRateSelectionLabel.sizeToFit()
        historyDaysSelectionLabel.sizeToFit()
        historyCurrenciesSelectionLabel.sizeToFit()
    }
    
    @objc func selectHistoryCurrencies() {
        let table = settingsViewModel!.initializeMultiSelectionTableViewController()
        pushViewController(viewController: table)
    }
    
    //MARK:- PickerView methods
    
    @objc func selectCurrency() {
        pickerMode = Constants.PickerModes.currency
        
        if let idx = AppSession.sharedSession.currencies.firstIndex(of: AppSession.sharedSession.baseCurrency) {
            pickerView.selectRow(idx, inComponent: 0, animated: false)
        }
        showPicker()
    }

    @objc func selectRefreshRate() {
        pickerMode = Constants.PickerModes.refreshRate
        showPicker()
    }
    
    @objc func selectHistoryDays() {
        pickerMode = Constants.PickerModes.historyDays
        showPicker()
    }
    
    func showPicker() {
        pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.2) {
            self.pickerContainerView.frame.origin.y = self.view.frame.size.height - self.pickerContainerView.frame.size.height
        }
    }
    
    func hidePicker() {
        UIView.animate(withDuration: 0.2) {
            self.pickerContainerView.frame.origin.y = self.view.frame.size.height
        }
    }
    
    @IBAction func didTapCancelPicker(sender: UIBarButtonItem) {
        hidePicker()
    }
    
    @IBAction func didTapDonePicker(sender: UIBarButtonItem) {
        let title = pickerView(pickerView, titleForRow: pickerView.selectedRow(inComponent: 0), forComponent: 0)!

        if pickerMode == Constants.PickerModes.currency {
            //if the user selects as base currency one that was used for the history chart, then swap out the old base with the new base in the history array
            settingsViewModel!.setNewBaseCurrency(currencyName: title)
            
            historyCurrenciesSelectionLabel.text = settingsViewModel!.getHistoryCurrenciesString()
            historyCurrenciesSelectionLabel.sizeToFit()
            baseCurrencySelectionLabel.text = title
            baseCurrencySelectionLabel.sizeToFit()
        }
        else if pickerMode == Constants.PickerModes.refreshRate {
            settingsViewModel!.setNewRefreshRate(refreshRate: title)
            
            refreshRateSelectionLabel.text = settingsViewModel!.getRefreshRateString()
            refreshRateSelectionLabel.sizeToFit()
        }
        else if pickerMode == Constants.PickerModes.historyDays {
            settingsViewModel!.setNewHistoryDays(historyDays: title)

            historyDaysSelectionLabel.text = settingsViewModel?.getHistoryDaysString()
            historyDaysSelectionLabel.sizeToFit()
        }
                
        hidePicker()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerMode {
        case Constants.PickerModes.currency:
            return AppSession.sharedSession.currencies.count
        case Constants.PickerModes.refreshRate:
            return refreshRates.count
        case Constants.PickerModes.historyDays:
            return historyDays.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerMode {
        case Constants.PickerModes.currency:
            return AppSession.sharedSession.currencies[row]
        case Constants.PickerModes.refreshRate:
            return refreshRates[row]
        case Constants.PickerModes.historyDays:
            return historyDays[row]
        default:
            return ""
        }
    }
}
