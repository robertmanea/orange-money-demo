//
//  HomePageViewController.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 30/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class HomePageViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timestampLabel: UILabel!
    
    var homePageViewModel: HomePageViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homePageViewModel = HomePageViewModel(homePageViewController: self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRates()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        homePageViewModel?.invalidateTimer()
    }
    
    override func arrangeViews() {
        super.arrangeViews()
        self.timestampLabel.frame.origin.y = homePageViewModel!.getBaseCurrencyLabelBottomY()
        self.tableView.frame.origin.y = homePageViewModel!.getTimestampLabelBottomY()
        self.tableView.frame.size.height = self.view.frame.size.height - self.tableView.frame.origin.y
    }
    
    func getRates() {
        homePageViewModel!.getRates(completion: { (success, message) in
            DispatchQueue.main.async {
                if success {
                    self.hideErrorLabel()
                    self.tableView.reloadData()
                }
                else {
                    self.errorLabel.text = message
                    self.showErrorLabel()
                }
                
                self.timestampLabel.text = self.homePageViewModel!.getLastUpdate()
            }
        })
    }
    
    // MARK:- UITableView methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homePageViewModel?.currencies.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CurrencyIdentifier"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        }
        
        cell?.textLabel?.text = homePageViewModel?.getRateString(forCurrencyAt: indexPath.row)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = .zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = .zero
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
}
