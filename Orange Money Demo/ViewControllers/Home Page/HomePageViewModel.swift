//
//  HomePageViewModel.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 08/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class HomePageViewModel: BaseViewModel {
    var homePageViewController: HomePageViewController
    var currencies = [Currency]()
    var timer: Timer?

    init(homePageViewController: HomePageViewController) {
        self.homePageViewController = homePageViewController
        super.init(baseViewController: homePageViewController)
    }
    
    func getRates(completion: @escaping (_ success: Bool, _ message: String?) -> Void) {
        Requests.getLatestExchangeRates { (currencies, message) in
            if currencies != nil {
                self.currencies = currencies!
                
                completion(true, nil)
            }
            else {
                completion(false, message)
            }
            
            self.timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(AppSession.sharedSession.homePageRefreshRate), repeats: false, block: { (_) in
                self.homePageViewController.getRates()
            })
        }
    }
    
    func getRateString(forCurrencyAt index: Int) -> String {
        if currencies.count <= index {
            return ""
        }
        
        let currency = currencies[index]
        return "\(currency.name) - \(currency.value)"
    }
    
    func getLastUpdate() -> String {
        let dF = DateFormatter()
        dF.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return "Last update: \(dF.string(from: Date()))"
    }
    
    func getTimestampLabelBottomY() -> CGFloat {
        return homePageViewController.timestampLabel.frame.origin.y + homePageViewController.timestampLabel.frame.size.height
    }
    
    func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }
}
