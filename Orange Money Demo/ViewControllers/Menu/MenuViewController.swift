//
//  MenuViewController.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 30/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var options = ["Home", "History", "Settings"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    @IBAction func didTapClose(_ sender: UIButton) {
        appDel.sideMenuController?.hideLeftViewAnimated()
    }
    
    // MARK:- UITableView methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MenuIdentifier"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        }
        
        cell?.textLabel?.text = options[indexPath.row]
        cell?.selectionStyle = .none
        cell?.textLabel?.textAlignment = .center
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = .zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = .zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            pushViewController(viewController: appDel.homePage)
            break
        case 1:
            let history = HistoryViewController(nibName: Constants.ViewControllers.history, bundle: nil)
            pushViewController(viewController: history)
            break
        case 2:
            let settings = SettingsViewController(nibName: Constants.ViewControllers.settings, bundle: nil)
            pushViewController(viewController: settings)
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
}
