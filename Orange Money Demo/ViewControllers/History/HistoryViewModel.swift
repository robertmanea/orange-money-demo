//
//  HistoryViewModel.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 08/12/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit
import Charts

class HistoryViewModel: BaseViewModel {
    var historyViewController: HistoryViewController
    var rates = [String:[Currency]]()
    var sortedDates = [String]()

    init(historyViewController: HistoryViewController) {
        self.historyViewController = historyViewController
        super.init(baseViewController: historyViewController)
    }
    
    func initChart() {
        historyViewController.chartView.chartDescription?.enabled = false
        
        historyViewController.chartView.leftAxis.enabled = false
        historyViewController.chartView.rightAxis.enabled = true
        
        historyViewController.chartView.drawBordersEnabled = true
        historyViewController.chartView.setScaleEnabled(true)
        historyViewController.chartView.dragEnabled = true
        historyViewController.chartView.pinchZoomEnabled = true
        
        let xAxis = historyViewController.chartView.xAxis
        xAxis.labelPosition = .top
        xAxis.labelRotationAngle = 60
        xAxis.labelFont = .systemFont(ofSize: 8, weight: .regular)
        xAxis.labelTextColor = .black
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = true
        xAxis.gridLineWidth = 0.1
        xAxis.drawLimitLinesBehindDataEnabled = true
        
        let leftAxis = historyViewController.chartView.leftAxis
        leftAxis.labelPosition = .insideChart
        leftAxis.labelFont = .systemFont(ofSize: 12, weight: .light)
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = true
        leftAxis.axisMinimum = 0
        leftAxis.axisMaximum = 170
        leftAxis.yOffset = -9
        leftAxis.labelTextColor = UIColor(red: 255/255, green: 192/255, blue: 56/255, alpha: 1)
        
        let l = historyViewController.chartView.legend
        l.horizontalAlignment = .right
        l.verticalAlignment = .top
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .square
        l.xEntrySpace = 4
    }
    
    func getHistory(completion: @escaping (_ success: Bool, _ message: String?) -> Void) {
        Requests.getHistory { (rates, message) in
            if rates != nil {
                self.rates = rates!
                
                completion(true, nil)
            }
            else {
                completion(false, message)
            }
            
            DispatchQueue.main.async {
                self.updateChartData()
            }
        }
    }
    
    func updateChartData() {
        let colors = ChartColorTemplates.vordiplom()[0...2]
        sortedDates = [String]()
        
        //the chart requires 3 datasets, one for each currency, so we need a structure that looks like this: {currency: {date: value}}
        let dataDict = rates.reduce([String:[String:Double]]()) { (dict, tuple) -> [String:[String:Double]] in
            let dateStr = tuple.key
            let currencies = tuple.value
            
            var mutableDict = dict
            for currency in currencies {
                if mutableDict[currency.name] == nil {
                    mutableDict[currency.name] = [String:Double]()
                }
                mutableDict[currency.name]![dateStr] = currency.value
            }
            
            return mutableDict
        }
        
        //setting min and max values for the y-axis, so that everything's visible
        //find the min and max for each currency, then take the min and max between them
        let minValue = dataDict.reduce(-1) { (res, tuple) -> Double in
            let datesAndValues = tuple.value
            let minForCurrency = datesAndValues.reduce(res) { (res2, valuesTuple) -> Double in
                return res2 > 0 ? (res2 > valuesTuple.value ? valuesTuple.value : res2) : valuesTuple.value
            }
            
            return res > 0 ? (res > minForCurrency ? minForCurrency : res) : minForCurrency
        }.rounded(.down)
        
        let maxValue = dataDict.reduce(-1) { (res, tuple) -> Double in
            let datesAndValues = tuple.value
            let minForCurrency = datesAndValues.reduce(res) { (res2, valuesTuple) -> Double in
                return res2 > 0 ? (res2 < valuesTuple.value ? valuesTuple.value : res2) : valuesTuple.value
            }
            
            return res > 0 ? (res < minForCurrency ? minForCurrency : res) : minForCurrency
        }.rounded(.up)
        
        historyViewController.chartView.leftAxis.axisMinimum = minValue
        historyViewController.chartView.leftAxis.axisMaximum = maxValue
        
        var colorIndex = 0
        let dataSets = dataDict.map({ (tuple) -> LineChartDataSet in
            let currencyStr = tuple.key
            let datesAndValues = tuple.value
            
            //the chart doesn't accept timestamps as x-axis values, so we'll have to pass indexes for the x-axis and then get the values from our sortedDates array through the IndexAxisValueFormatter
            var xValue = 0
            let values = datesAndValues.map({ (datesAndValuesTuple) -> ChartDataEntry in
                let dateStr = datesAndValuesTuple.key
                if !sortedDates.contains(dateStr) {
                    sortedDates.append(dateStr)
                }
                
                defer { xValue += 1 }
                return ChartDataEntry(x: Double(xValue), y: datesAndValuesTuple.value)
            }).sorted(by: { (e1, e2) -> Bool in
                return e1.x < e2.x
            })
            
            let set = LineChartDataSet(values: values, label: "\(currencyStr)")
            set.lineWidth = 2.5
            set.circleRadius = 4
            set.circleHoleRadius = 2
            let color = colors[colorIndex % colors.count]
            set.setColor(color)
            set.setCircleColor(color)
            set.valueFormatter = ExchangeRateValueFormatter()
            colorIndex += 1
            
            return set
        })
        
        sortedDates.sort { (d1, d2) -> Bool in
            return d1.compare(d2) == .orderedAscending
        }
        
        historyViewController.chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: sortedDates)
        historyViewController.chartView.xAxis.labelCount = sortedDates.count
        //a possible bug in the Charts framework causes the first values of the linechart to be displayed weirdly
        //insetting the axis makes sure nothing is displayed improperly and no values are overlapped with the margin of the chart
        historyViewController.chartView.xAxis.axisMinimum = -0.5
        historyViewController.chartView.xAxis.axisMaximum = Double(sortedDates.count) - 0.5
        
        let data = LineChartData(dataSets: dataSets)
        data.setValueFont(.systemFont(ofSize: 9, weight: .regular))
        historyViewController.chartView.data = data
        historyViewController.chartView.setNeedsDisplay()
        historyViewController.chartView.notifyDataSetChanged()
    }
}
