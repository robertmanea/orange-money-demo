//
//  HistoryViewController.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 30/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit
import Charts

class HistoryViewController: BaseViewController, ChartViewDelegate {
    @IBOutlet weak var chartView: LineChartView!
    
    var historyViewModel: HistoryViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        historyViewModel = HistoryViewModel(historyViewController: self)
        chartView.delegate = self
        historyViewModel!.initChart()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        historyViewModel?.getHistory(completion: { (success, message) in
            DispatchQueue.main.async {
                if success {
                    self.hideErrorLabel()
                }
                else {
                    self.errorLabel.text = message
                    self.showErrorLabel()
                }
            }
        })
    }
    
    override func arrangeViews() {
        super.arrangeViews()
        self.chartView.frame.origin.y = self.baseCurrencyLabel.frame.origin.y + self.baseCurrencyLabel.frame.size.height
        self.chartView.frame.size.height = self.view.frame.size.height - self.chartView.frame.origin.y
    }
}
