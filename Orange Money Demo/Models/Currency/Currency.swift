//
//  Currency.swift
//  Orange Money Demo
//
//  Created by Robert Manea on 26/11/2019.
//  Copyright © 2019 Robert Manea. All rights reserved.
//

import UIKit

enum CodingKeys: String, CodingKey {
    case rates = "rates"
}

struct DynamicKey: CodingKey {
    var intValue: Int? { return nil }
    init?(intValue: Int) { return nil }
    
    var stringValue: String
    init?(stringValue: String) {
        self.stringValue = stringValue
    }
}

struct RatesResponse: Decodable {
    var currencies: [Currency]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: DynamicKey.self, forKey: .rates)
        
        self.currencies = [Currency]()
        try nestedContainer.allKeys.forEach({ (key) in
            let name = key.stringValue
            let value = try nestedContainer.decode(Double.self, forKey: key)
            self.currencies.append(Currency(name: name, value: value))
        })
    }
}

struct HistoryResponse: Decodable {
    var historyDict: [String:[Currency]]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dateContainer = try container.nestedContainer(keyedBy: DynamicKey.self, forKey: .rates)
        
        //we have to add values for the weekend days that the chart will display, so we'll just create a new array with all the necessary dates
        var dates = [String]()
        let days = AppSession.sharedSession.historyDays
        let dF = DateFormatter()
        dF.dateFormat = "yyyy-MM-dd"
        let calendar = Calendar.current
        for i in 1...days {
            dates.append(dF.string(from: calendar.date(byAdding: .day, value: -i, to: Date())!))
        }
        
        self.historyDict = [String:[Currency]]()
        
        for date in dates {
            var currencyContainer: KeyedDecodingContainer<DynamicKey>? = nil
            if dateContainer.contains(DynamicKey.init(stringValue: date)!) {
                currencyContainer = try dateContainer.nestedContainer(keyedBy: DynamicKey.self, forKey: DynamicKey.init(stringValue: date)!)
            }
            else {
                //if no entry could be found for the respective date, find the entry for the first date in the dictionary that precedes to ours
                let key = dateContainer.allKeys.filter({ (key) -> Bool in
                    return key.stringValue.compare(date) == .orderedAscending
                }).sorted(by: { (key1, key2) -> Bool in
                    return key1.stringValue.compare(key2.stringValue) == .orderedDescending
                }).first
                
                if key != nil {
                    currencyContainer = try dateContainer.nestedContainer(keyedBy: DynamicKey.self, forKey: key!)
                }
            }
            
            if currencyContainer != nil {
                var dateCurrencies = [Currency]()
                try currencyContainer!.allKeys.forEach({ (key) in
                    let name = key.stringValue
                    let value = try currencyContainer!.decode(Double.self, forKey: key)
                    dateCurrencies.append(Currency(name: name, value: value))
                })
                
                self.historyDict.updateValue(dateCurrencies, forKey: date)
            }
        }
    }
}

class Currency: Decodable {
    let name: String
    let value: Double
    
    init(name: String, value: Double) {
        self.name = name
        self.value = value
    }
}
